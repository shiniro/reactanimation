import React from 'react';
import CSSTransition from 'react-transition-group/CSSTransition'

import './Modal.css';

const animationTiming = {
    enter: 400,
    exit: 1000
}

const modal = (props) => {

    return (
        <CSSTransition
          in={props.show}
          timeout={animationTiming}
          mountOnEnter // add to the dom if exist
          unmountOnExit // remove from the dom
          //classNames="fade-slide"
          classNames={{
              enter: '',
              enterActive: 'ModalOpen',
              exit: '',
              exitActive: 'ModalClosed'
              // appear: ,
              // appearActive: ,
          }}
        >
            <div className="Modal" >
                <h1>A Modal</h1>
                <button className="Button" onClick={props.closed}>Dismiss</button>
            </div>
        </CSSTransition>
        // <Transition
        //   in={props.show}
        //   timeout={animationTiming}
        //   mountOnEnter // add to the dom if exist
        //   unmountOnExit // remove from the dom
        // >
        // {
        //     state => {

        //         const cssClasses = [
        //             'Modal',
        //             state === 'entering' 
        //                 ? 'ModalOpen' 
        //                 : state === 'exiting' 
        //                     ? 'ModalClosed' 
        //                     : null]

        //         console.log(cssClasses);

        //         return (
        //             <div className={cssClasses.join(' ')}>
        //                 <h1>A Modal</h1>
        //                 <button className="Button" onClick={props.closed}>Dismiss</button>
        //             </div>
        //         )
        //     }
        // }
            
        // </Transition>
    );
};

export default modal;